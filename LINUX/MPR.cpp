#include "MPR.h"

MPR::MPR(){

}

void MPR::CRCH(){//Create child
	signal(SIGCHLD, SIG_IGN);//Prevent zombie creation
	pid.push_back(fork());
	if(pid[pid.size()-1]==0){//slave
			PrN++;
		}
	else if(pid[pid.size()-1]>0){//Master
			PrN=-1;//Sets de master initial proces number to a negative value, so it dont do work
			//Wait for proceses to finish

		}
}

void MPR::WFCH(int mcm){//Master waits for childrens here until end of execution.
	MCM=mcm;
	if(PrN==-1){//If master process executes this it waits here
		CHLDEX.clear();
		for(int i=0;i<nPr;i++){
			waitpid(pid[i], &status, WUNTRACED);
			CHLDEX.push_back(status);

			if(MCM==1){
				for(uint ii=i+1;i<nPr;ii++){
					kill(pid[ii], SIGTERM);

				}
			}

		}
		//pid.clear();
	}

}


void MPR::CSPS(int NpR){//Creates subproces structure
	nPr=NpR;
	pid.clear();
	for(int i=0;i<nPr;i++){
		PrN=i;
		pid.push_back(fork());
		if(pid[i]==0){//slave
			break;
		}
		else if(pid[i]>0){//Master
			PrN=-1;//Sets de master initial proces number to a negative value, so it dont do work
			//Wait for proceses to finish

		}
		else{
			std::cout<<"FORK ERROR"<<std::endl;
		}
	}


}



int MPR::PRN(){//Retrurn the treat id, -1 for master.
	return PrN;
}
