#ifndef MPR_HPP_INCLUDED
#define MPR_HPP_INCLUDED
//Multi proces

#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <sys/wait.h>


class MPR {//Multiproces lib
    int MCM=0;//Master control mode// 0 Waits for all childs to finish, 1 kills all childs if the first created finish
    int nPr=1;//Fork number of subproceses; 1 is necesary because the master treat keeps block until treat finish, in this case treat 0 is the proces how makes the work. 
    int PrN=0;//Proces number, used as work distributor number the childrens from 0 to n
    int status;
    std::vector<pid_t> pid;//Pid for fork;
    
    public:
        
    std::vector <int> CHLDEX;//Vector with the exit code of the thread on the same order as PrN
        
    MPR();
    
    void CRCH();//Create child
    
    void WFCH(int mcm=0);//Master waits for childrens here until end of execution.

    void CSPS(int NpR=1);//Creates subproces structure.

    int PRN();//Retrurn the treat id, -1 for master.

};

#endif
