#include "SML.h"

SML::SML(){}

SML::SML(key_t SMKEYi,int SMSZi){//Llave de la memoria y espacio de la memoria compartida

	SMKEY=SMKEYi;
	SMSZ=SMSZi;

	//Shared memori init
	SMID=shmget(SMKEY,SMSZ,IPC_CREAT | 0666);
	if(SMID<0){
		std::cerr<<"SML::SML::ERROR::semget::1\n";
		exit(1);
	}

	SMDT=(char*)shmat(SMID,0,0);
	if(SMDT==(char*)-1){
		std::cerr<<"SML::SML::ERROR::shmat::1\n";
		exit(1);
	}

SYNC=SNC(SMKEYi); //Semaphore creation

}

SML::SML(const SML& sml){//Duplicador de clase
	SYNC=sml.SYNC;

	SMID=sml.SMID;//Shared memory id
	SMDT=sml.SMDT;//Shared memory data *
	SMPNT=sml.SMPNT;//Shared memory pointer *
	SMSZ=sml.SMSZ;//Shared memory size in bytes

	SB=sml.SB;//Bloqueo de seguridad, para aplicaciones en las que predomine la seguridad, y la integridad de los datos

	SMSTR=sml.SMSTR;//Shared memory string

	SMKEY=sml.SMKEY; //Shared memory key
}

SML& SML::operator=(const SML& sml){
	SYNC=sml.SYNC;

	SMID=sml.SMID;//Shared memory id
	SMDT=sml.SMDT;//Shared memory data *
	SMPNT=sml.SMPNT;//Shared memory pointer *
	SMSZ=sml.SMSZ;//Shared memory size in bytes

	SB=sml.SB;//Bloqueo de seguridad, para aplicaciones en las que predomine la seguridad, y la integridad de los datos

	SMSTR=sml.SMSTR;//Shared memory string

	SMKEY=sml.SMKEY; //Shared memory key

	return *this;
}

// SML::~SML(){
//
// }


std::string SML::RM(int block){//Read memory. If block > 0 process stops until semaphore is freed
	//SMSTR.clear();//Clean shared memory archived data
	if(block>0){
		//std::cout<<"SML::RM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
		SYNC.SV(0,1,1);//Block semaphore, wait until unlock
		//std::cout<<"SML::RM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;

		//READ SHARED MEMORY
		SMSTR=SMDT;
		// for(SMPNT=SMDT;*SMPNT!=0;SMPNT++){
		// 	SMSTR+=*SMPNT;
		// }

	}
	else if(SYNC.RV(0,0)==0){//If semaphore is non blocked
		SYNC.SV(0,1);//Block semaphore
		//READ SHARED MEMORY

		SMSTR=SMDT;
		// for(SMPNT=SMDT;*SMPNT!=0;SMPNT++){
		// 	SMSTR+=*SMPNT;
		// }

	}
	//std::cout<<"SML::RM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
	int SYNCSV=SYNC.RV(0,0);//Non block read
	SYNC.SV(0,-SYNCSV);//Free semaphore
	//std::cout<<"SML::RM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl<<std::endl;;

return SMSTR;
}

std::vector<char> SML::RMB(int block){//Read memory. If block > 0 process stops until semaphore is freed
	//SMSTR.clear();//Clean shared memory archived data
	if(block>0){
		//std::cout<<"SML::RMB::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
		SYNC.SV(0,1,1);//Block semaphore, wait until unlock
		//std::cout<<"SML::RMB::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;

		//READ SHARED MEMORY
		// SMBD.clear();
		// std::cout<<"SML::RMB::SMBD.clear()"<<std::endl;
		// SMBD.resize(SMSZ);
		// std::cout<<"SML::RMB::SMBD.resize(SMSZ)"<<std::endl;
		// std::vector<char> vec(SMDT, SMDT + SMSZ);
		// std::cout<<"SML::RMB::std::vector<char> vec(SMDT, cstr + SMSZ)"<<std::endl;
		// SMBD=vec;
		// //memcpy(&SMBD[0],SMDT,SMSZ);
		if (SMDT == nullptr) std::cout<<"SML::RMB::nullptr ERROR"<<std::endl;

		//std::cout<<"SML::RMB::SMSZ="<<SMSZ<<std::endl;
		//std::cout<<"SML::RMB::strlen(SMDT)="<<std::strlen(SMDT)<<std::endl;
		//SMBD.clear();
		//SMBD.resize(SMSZ);
		SMBD=std::vector<char> (SMDT, SMDT +SMSZ);
		//std::cout<<"SML::RMB::MEMORY READED"<<std::endl;

		// for(SMPNT=SMDT;*SMPNT!=0;SMPNT++){
		// 	SMSTR+=*SMPNT;
		// }

	}
	else if(SYNC.RV(0,0)==0){//If semaphore is non blocked
		SYNC.SV(0,1);//Block semaphore
		//READ SHARED MEMORY

		SMBD=std::vector<char> (SMDT, SMDT + SMSZ);
		// for(SMPNT=SMDT;*SMPNT!=0;SMPNT++){
		// 	SMSTR+=*SMPNT;
		// }

	}
	//std::cout<<"SML::RM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
	int SYNCSV=SYNC.RV(0,0);//Non block read
	SYNC.SV(0,-SYNCSV);//Free semaphore
	//std::cout<<"SML::RM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl<<std::endl;;

return SMBD;
}

void SML::WM(std::string data, int block){//Write memory
	if(block>0){
		//std::cout<<"SML::WM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
		SYNC.SV(0,1,1);//Block semaphore, wait until unlock
		//std::cout<<"SML::WM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
		//Write data
		strncpy(SMDT,data.c_str(),SMSZ);

	}
	else if(SYNC.RV(0,0)==0){//If semaphore is non blocked
		SYNC.SV(0,1);//Block semaphore
		//Write data
		strncpy(SMDT,data.c_str(),SMSZ);

	}

	//std::cout<<"SML::WM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
	int SYNCSV=SYNC.RV(0,0);//Non block read
	SYNC.SV(0,-SYNCSV);//Free semaphore
	//std::cout<<"SML::WM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl<<std::endl;;

}

void SML::WM(std::vector<char> data, int block){//Write memory
	assert(data.size()<SMSZ);
	if(block>0){
		//std::cout<<"SML::WM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
		SYNC.SV(0,1,1);//Block semaphore, wait until unlock
		//std::cout<<"SML::WM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
		//Write data
		//SMDT=&data[0];
		std::memcpy(SMDT, data.data(), data.size());
		//std::memcpy(SMDT, data.data(), SMSZ);

	}
	else if(SYNC.RV(0,0)==0){//If semaphore is non blocked
		SYNC.SV(0,1);//Block semaphore
		//Write data
		//SMDT=&data[0];
		std::memcpy(SMDT, data.data(), data.size());
		//std::memcpy(SMDT, data.data(), SMSZ);

	}

	else{
		//std::cout<<"SML::WM::DATA_WRITEN"<<std::endl;
	}

	//std::cout<<"SML::RMB::strlen(SMDT)="<<std::strlen(SMDT)<<std::endl;

	//std::cout<<"SML::WM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl;
	int SYNCSV=SYNC.RV(0,0);//Non block read
	SYNC.SV(0,-SYNCSV);//Free semaphore
	//std::cout<<"SML::WM::SYNC.RV(0,0)="<<SYNC.RV(0,0)<<std::endl<<std::endl;;

}
