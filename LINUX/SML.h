#ifndef SML_H_INCLUDED
#define SML_H_INCLUDED

//Shared memory library

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <errno.h>
#include <iostream>

#include <cassert>


#include "SNC.h"

class SML{

	SNC SYNC;

	int SMID;//Shared memory id
	char *SMDT;//Shared memory data
	char *SMPNT;//Shared memory pointer

	public:

	int SMSZ=2048;//Shared memory size in bytes

	bool SB=false;//Bloqueo de seguridad, para aplicaciones en las que predomine la seguridad, y la integridad de los datos

	std::string SMSTR;//Shared memory string
	std::vector<char> SMBD;//Shared memory binary data

	key_t SMKEY; //Shared memory key


	SML();

	SML(key_t SMKEYi,int SMSZi);

	SML(const SML& sml);//Duplicador de clase

	SML& operator=(const SML& sml);

	// ~SML();


	std::string RM(int block=0);//Read memory. If block > 0 process stops until semaphore is freed
	std::vector<char> RMB(int block=0);//Read memory bynary. If block > 0 process stops until semaphore is freed

	void WM(std::string data, int block=0);//Write memory
	void WM(std::vector<char> data, int block=0);//Write memory

};




#endif // SML_H_INCLUDED
