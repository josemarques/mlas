#include "SNC.h"

//El semaforo de sincronización se bloquea si no está en 0, el valor del semaforo ha de estar siempre por encima de 0, positivos para bloquear, este es un uso especial del semaforo para sincronización.

SNC::SNC(){}

SNC::SNC(key_t semkeyi,int nsems){//KEY for SEMAPHORE, NUMBER OF SEMAPHORES

	semkey=semkeyi;
	//int semid;
	struct semid_ds bufs;

	union semuns args;
	struct sembuf sbs;

	sbs.sem_op = -1;
	sbs.sem_flg = SEM_UNDO;//

	//sbs.sem_flg = 0;
	args.vals = 1;

	semid = semget(semkey, nsems, IPC_CREAT | IPC_EXCL | 0666);

	if (semid >= 0) { /* we got it first */
		sbs.sem_op = 1; //sbs.sem_flg = 0;
		args.vals = 1;

		for(sbs.sem_num = 0; sbs.sem_num <nsems; sbs.sem_num++) {
			//do a semop() to "free" the semaphores.
			// this sets the sem_otime field, as needed below.
			if (semop(semid, &sbs, 1) == -1) {
				int e = errno;
				semctl(semid, 0, IPC_RMID); // clean up
				errno = e;
				//return -1; // error, check errno
			}
		}
	}

	else if (errno == EEXIST) { /* someone else got it first */
		int ready = 0;

		semid = semget(semkey, nsems, 0); /* get the id */
		//if (semid < 0) return semid; /* error, check errno */

		/* wait for other process to initialize the semaphore: */
		args.bufs = &bufs;
		for(int i = 0; i < MAX_RETRIESS && !ready; i++) {
			semctl(semid,nsems, IPC_STAT, args);
			if (args.bufs->sem_otime != 0) {
				ready = 1;
			}

			else {
				sleep(1);
			}
		}

		if (!ready) {
			errno = ETIME;
			//return -1;
		}
	}
	//return semid;
}

SNC::SNC(const SNC& sc){
	bufs=sc.bufs;
	args=sc.args;
	sbs=sc.sbs;
	semkey=sc.semkey;
	semid=sc.semid;
	nsm=sc.nsm;
}//Duplicador de clase

SNC& SNC::operator=(const SNC& sc){
	bufs=sc.bufs;
	args=sc.args;
	sbs=sc.sbs;
	semkey=sc.semkey;
	semid=sc.semid;
	nsm=sc.nsm;
	return *this;
} //Igualador de clase

// SNC::~SNC(){
// 	//TODO Destroy semaphore if is not in use.
// }//Destructor

	//Base functionality

int SNC::RV(int nsm, int block){//Read value. Block > 0 block the execution until nsm semaphore is freed
	if(block>0){
		sbs.sem_flg = SEM_UNDO;//Pausa hasta que se libera el proceso
	}

	else{
		sbs.sem_flg = IPC_NOWAIT;//Non blocking
	}

	int SEMV=0;

	//int sen=0;
	sbs.sem_op = 0;//read
	sbs.sem_num = nsm;

	if (semop(semid, &sbs, 1) == -1) {
			//std::cout<<"SNC.h -> SNC.RV -> SEM READ ERROR"<<std::endl;
		//Sem blocked
		SEMV=semctl(semid, nsm, GETVAL);
		//return 1;
	}//END BLOCK SHM IN

	//int SEMV=semctl(semid, nsm, GETVAL);
	return SEMV;
}

void SNC::SV(int nsm, short value, int block){//Set value. Block > 0 block the execution until nsm semaphore is freed
	if(block>0){
		sbs.sem_flg = SEM_UNDO;//Pausa hasta que se libera el proceso
	}

	else{
		sbs.sem_flg = IPC_NOWAIT;//Non blocking
	}
	//int sen=0;
	sbs.sem_op = value;// value==1 increment semaphore blocking it, value==-1 decrement semaphore freeing it.
	sbs.sem_num = nsm;

	if (semop(semid, &sbs, 1) == -1) {
			//std::cout<<"SEM ERROR BLOCK"<<std::endl;//Semaphore is blocked and flag is IPC_NOWAIT
		//return 1;
	}//END BLOCK SHM IN
}
