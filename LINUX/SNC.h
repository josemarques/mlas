#ifndef SNC_H_INCLUDED
#define SNC_H_INCLUDED

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

#include <unistd.h>
#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <errno.h>
#include <iostream>

#define MAX_RETRIESS 50

//El semaforo de sincronización se bloquea si no está en 0, el valor del semaforo ha de estar siempre por encima de 0, positivos para bloquear, este es un uso especial del semaforo para sincronización.

class SNC{//SYNC based on System V semaphore

	union semuns {
	int vals;			   /* used for SETVAL only */
	struct semid_ds *bufs;  /* used for IPC_STAT and IPC_SET */
	ushort *arrays;		 /* used for GETALL and SETALL */
	};

	/*union semun {
    int              vals;    // Value for SETVAL
    struct semid_ds *bufs;    // Buffer for IPC_STAT, IPC_SET
    unsigned short  *arrays;  // Array for GETALL, SETALL
    struct seminfo  *__buf;  // Buffer for IPC_INFO
							//  (Linux-specific)  };*/

	struct semid_ds bufs;
	union semuns args;
	struct sembuf sbs;

	key_t semkey=5800;
	int semid;

	int nsm;//Number of semaphores

	public:
	SNC();

	SNC(key_t semkeyi,int nsems=1);
	SNC(const SNC& sc);//Duplicador de clase

	SNC& operator=(const SNC& sc);

	// ~SNC();	//TODO Destroy semaphore if is not in use.

	//Base functionality

	int RV(int nsm, int block=0);//Read value. Block > 0 block the execution until nsm semaphore is freed


	void SV(int nsm, short value, int block=0);//Set value. Block > 0 block the execution until nsm semaphore is freed

};


#endif // SNC_H_INCLUDED
