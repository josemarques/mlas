#ifndef MLAS_H_INCLUDED
#define MLAS_H_INCLUDED

//https://stackoverflow.com/questions/430424/are-there-any-macros-to-determine-if-my-code-is-being-compiled-to-windows
//Platform especific definitions
#ifdef __APPLE__
#define OS_APPLE
#endif

#ifdef __MINGW32__
#define OS_WIN
#endif

#ifdef __linux__
#define OS_LINUX
#endif

#ifdef __ANDROID__
#define OS_ANDROID
#endif

#if defined(_WIN32) || defined(WIN32) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)
#define OS_WIN
#endif

#ifdef OS_WIN
//Windows specific
#include "WINDWS/SNC.h"
#include "WINDWS/SML.h"

#endif
//Normal

#ifdef OS_LINUX
#include "LINUX/SNC.h" //POSIX SEMAPHORES. HI-PERFORMANCE-SERVER-ORIENTED-SYNC
#include "LINUX/SML.h" //Shared memory
#include "LINUX/MPR.h" //Multiproces handler
#endif

//Platfor especific definitions

class MLAS{  //MLAS class defines an object that represent a compute threat that can replicate it self, sincronize it function with others and share data in runtime or at the end of execution throug ram, files, UDP and TCP. An encription function is yet to be implemented
  
private:

public:

    MLAS();//Constructor sin estado
    MLAS(int memid);//Constructor con estado
    MLAS(const MLAS& nd);//Duplicador de clase
    MLAS& operator=(const MLAS& nd); //Igualador de clase
    ~MLAS();//Destructor

    void SETP();
};


#endif 
