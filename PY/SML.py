#!/usr/bin/env python

#http://semanchuk.com/philip/sysv_ipc/
import sysv_ipc

import SNC #Sync import to control shared memory access

#http://semanchuk.com/philip/posix_ipc/
#import posix_ipc

class SML:
	#TODO Multiple semaphores pero object. SYSV functionality
	def INIT(self, smkey, smsize=0):#Initialization
		try:
			self.SM=sysv_ipc.SharedMemory(smkey, flags = sysv_ipc.IPC_CREX, size = smsize)
		except sysv_ipc.ExistentialError:
			#Memory already exist
			self.SM=sysv_ipc.SharedMemory(smkey, flags = 0, size = smsize)
		else:
			pass


	def TERM(self):#Termination
		if self.SM:
			self.SM=self.SM.remove()


	def __init__(self, smkey=5801, smsize=1024):
		#
		self.KEY=int(smkey)##Shared memory ans sync semaphore key. Same for both.
		self.SIZE=int(smsize)#Shared memory size in bytes

		self.SEM=SNC.SNC(self.KEY)#Semaphore initialization

		self.INIT(self.KEY,self.SIZE)#Shared memory initialization

		self.WDATA=""#Last writed data
		self.RDATA=""#Last readed data

	#Base functionality

	def RM(self, block=0):
		#Base read memory functionality. Block > 0 block the process until the memory is free to read
		self.SEM.SV(1,block)#Blocks the semaphore
		self.RDATA=str(self.SM.read(self.SIZE))#Reads the data
		self.SEM.SV(-1,block)#Free the semaphore
		return(self.RDATA)


	def WM(self, data, block=0):
		#Base write memory functionality. Block > 0 block the process until the memory is free to read
		self.SEM.SV(1,block)#Blocks the semaphore
		self.SM.write(str(data))#Writes the data
		self.SEM.SV(-1,block)#Free the semaphore
		self.WDATA=str(data)
