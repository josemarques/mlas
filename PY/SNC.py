#!/usr/bin/env python

#http://semanchuk.com/philip/sysv_ipc/
import sysv_ipc

#http://semanchuk.com/philip/posix_ipc/
#import posix_ipc

class SNC:
	#TODO Multiple semaphores pero object. SYSV functionality
	def INIT(self, semkey):#Initialization
		try:
			self.SEM = sysv_ipc.Semaphore(self.KEY, sysv_ipc.IPC_CREX, initial_value=0)
		except sysv_ipc.ExistentialError:
			#Semaphore already exist
			self.SEM = sysv_ipc.Semaphore(self.KEY, initial_value=0)
		else:
			pass


	def TERM(self):#Termination
		if self.SEM:
			self.SEM=self.SEM.remove()
            #self.SEM.unlink()

	def __init__(self, semkey=5800):
		#
		self.KEY=int(semkey)
		self.INIT(self.KEY)

		self.SEMVAL=self.SEM.value

	#Base functionality

	def RV(self, block=0):
		#Base read semaphore functionality. Block > 0 block the process until the semaphore is freed
		if(block>0): TIMOUT=None
		else: TIMOUT=0

		try:
			self.SEM.Z(TIMOUT)
		except sysv_ipc.BusyError:
			pass #Blocked semaphore

		self.SEMVAL=self.SEM.value

		return self.SEMVAL

	def SV(self, value, block=0):
		#Base set semaphore value functionality. Block > 0 block the process until the semaphore is freed
		if(block>0): TIMOUT=None
		else: TIMOUT=0

		if(value > 0):
			self.SEM.release(value) #Unblock semaphore
		else:
			try:
				self.SEM.acquire(TIMOUT, value) #Blocks semaphore
			except sysv_ipc.BusyError:
				pass #Already blocked semaphore
	#Wrapper

	def R(self):
		#//Read non bloking. Lee el semaforo dejamdo el proceso bloqueado hasta que se libera el semaforo, se libera cuando su valor es 0
		return -1
		
	def W(self):
		#//Read blocking.Wait. Process wait until semaphore is freed, se libera cuando su valor es 0
		return -1

	def B(self):
		#Blocks the semaphore. No wait
		return -1

	def F(self):
		#Free the semaphore. No wait
		return -1
