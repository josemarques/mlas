// NANOS_DSKTP.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
// Test for use comprobation and example aplication

//#include "../MLAS.h"
#include "../LINUX/SNC.h"

int main(){
    std::cout << "SNC_TST"<<std::endl;
    
    //SNC TEST
    std::cout<<"SNC CREATION"<<std::endl;
	SNC SYNCSEM(5800,1);

	//std::cout<<"SEM KEY::"<<SYNCSEM.semkey<<std::endl;

	//Base functionality test
	std::cout<<"READ SEM"<<std::endl;
	int SEMVAL=SYNCSEM.RV(0);//Read value of semaphore 0 non block
	std::cout<<"SEMVAL::"<<SEMVAL<<std::endl;

	//Put semaphore to 0
	std::cout<<"UNBLOCK SEM"<<std::endl;
	SYNCSEM.SV(0,-SEMVAL);//Unblock semaphore
	//SYNCSEM.F(0);

	std::cout<<"READ SEM"<<std::endl;
	SEMVAL=SYNCSEM.RV(0);//Read value of semaphore 0 non block
	std::cout<<"SEMVAL::"<<SEMVAL<<std::endl;

	std::cout<<"BLOCK SEM"<<std::endl;
	SYNCSEM.SV(0,1);//Block semaphore 0 with an operation of 1


	std::cout<<"READ SEM"<<std::endl;
	SEMVAL=SYNCSEM.RV(0);//Read value of semaphore 0 non block
	std::cout<<"SEMVAL::"<<SEMVAL<<std::endl;

	std::cout<<"READ SEM BLOCKING"<<std::endl;
	SEMVAL=SYNCSEM.RV(0,1);//Read value of semaphore 0 block
	std::cout<<"SEMVAL::"<<SEMVAL<<std::endl;

    std::cout<<""<<std::endl;
    
    
    
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
