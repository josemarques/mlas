// Test for use comprobation and example aplication

//#include "../MLAS.h"
#include "../LINUX/SML.h"

int main(){
    std::cout << "SML_TST"<<std::endl;
    
    //SNC TEST
    std::cout<<"SML CREATION"<<std::endl;
	SML SHMEM(5801,1024);

	//std::cout<<"SEM KEY::"<<SYNCSEM.semkey<<std::endl;

	//Base functionality test
	std::cout<<"READ SM"<<std::endl;
	std::string SMDATA=SHMEM.RM();//Read data of shared memeory 0 non block
	std::cout<<SMDATA<<std::endl;

	std::cout<<"WRITE SM"<<std::endl;
	SHMEM.WM("SM::C++::TEST");

	std::cout<<"READ SM"<<std::endl;
	SMDATA=SHMEM.RM();//Read data of shared memeory 0 non block
	std::cout<<SMDATA<<std::endl;
    
    
    
}
