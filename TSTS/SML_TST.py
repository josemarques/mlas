#!/usr/bin/env python
import sys
sys.path.append('../PY/')

import SML

def TEST():
	print("INIT_SML")
	SMEM=SML.SML()

	ENCOD = 'utf-8'#Shared memory binary data enconding

	print("SEAD_SM")
	print(SMEM.RM(1))#Waits until SM is free
	print("WRITE_SM")
	SMEM.WM("SM::PYTHON::TEST",1)#Waits until SM is free
	print("SEAD_SM")
	print(SMEM.RM(1))#Waits until SM is free



if __name__ == '__main__':
	TEST()
