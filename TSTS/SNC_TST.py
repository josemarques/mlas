#!/usr/bin/env python
import sys
sys.path.append('../PY/')

import SNC

def TEST():
	SYNC=SNC.SNC()

	#Free semaphore
	while(SYNC.SEMVAL>0):
		SYNC.SV(-1)
		SYNC.RV()


	print(SYNC.RV())
	SYNC.SV(1)
	print(SYNC.RV())
	SYNC.SV(-1)
	print(SYNC.RV())

	print("MP BLOCK TEST")
	SYNC.SV(1)
	print("Wait until other process unblocks the semaphore")
	print("Execute again this test in another console")
	print(SYNC.RV(1))


if __name__ == '__main__':
	TEST()
