#ifndef SML_H_INCLUDED
#define SML_H_INCLUDED

#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <errno.h>
#include <iostream>

#include <vector>

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#pragma comment(lib, "user32.lib")

#include <atlstr.h>


//https://docs.microsoft.com/en-us/windows/win32/memory/creating-named-shared-memory

class SMO{ //Shared memory object class
private:

	int SMKEY = 4432131;

	int SM_SIZE = 1024;

	HANDLE hMapFile;
	LPCTSTR pBuf;
	//TCHAR szName[] = TEXT("u%", 4432131);
	//TCHAR szMsg[] = TEXT("Message from first process.");


	bool SUCCS = false;

public:

	std::string TXMSG;
	std::string RXMSG;

	SMO() {
		SETP();
	}//Constructor sin estado
	SMO(int smkey) {
		SMKEY = smkey;
		SMO();
	}//Constructor con estado
	SMO(const SMO& smo) {
	}//Duplicador de clase
	SMO& operator=(const SMO& smo) {
	} //Igualador de clase
	~SMO() {
		//UnmapViewOfFile(pBuf);
		//CloseHandle(hMapFile);
	}//Destructor

	void SETP() {//Initialize the SMO in case that has'n been constructed in the declaration
		std::string KEY = std::to_string(SMKEY);
		static const int sSize = KEY.size() + 1;
		TCHAR szName[/*sSize*/1024];
		_tcscpy_s(szName, CA2T(KEY.c_str()));


		hMapFile = OpenFileMapping(
			FILE_MAP_ALL_ACCESS,   // read/write access
			FALSE,                 // do not inherit the name
			szName);

		SUCCS = true;

		if (hMapFile == NULL) {
			//std::cout << "Could not open file mapping object " << GetLastError() << std::endl;
			SUCCS = false;

			//std::cout << "Creating file mapping" << std::endl;
			hMapFile = CreateFileMapping(
				INVALID_HANDLE_VALUE,    // use paging file
				NULL,                    // default security
				PAGE_READWRITE,          // read/write access
				0,                       // maximum object size (high-order DWORD)
				SM_SIZE,                // maximum object size (low-order DWORD)
				szName);
			// name of mapping object
			SUCCS = true;
		}

			if (hMapFile == NULL) {
				//std::cout << "Could not create file mapping object " << GetLastError() << std::endl;
				SUCCS = false;
			}

		if(SUCCS){//If creation of file mapping is succesfull
				pBuf = (LPTSTR)MapViewOfFile(hMapFile,   // handle to map object
					FILE_MAP_ALL_ACCESS, // read/write permission
					0,
					0,
					SM_SIZE);
				SUCCS = true;
			}

			if (pBuf == NULL) {
				//std::cout << "Could not map view of file " << GetLastError() << std::endl;
				CloseHandle(hMapFile);
				SUCCS = false;
			}
		
	}

	bool ERRCHK() {//true if error is detected.
		if (hMapFile == NULL)
		{
			std::cout << "Could not open or create file mapping object " << GetLastError() << std::endl;
			//_tprintf(TEXT("Could not open file mapping object (%d).\n"),GetLastError());
			return true;
		}

		if (pBuf == NULL)
		{
			std::cout << "Could not map view of file " << GetLastError() << std::endl;
			//_tprintf(TEXT("Could not map view of file (%d).\n"),
				//GetLastError());

			CloseHandle(hMapFile);

			return true;
		}
		return false;
	}
	void CLOSE() {
		UnmapViewOfFile(pBuf);
		CloseHandle(hMapFile);
	}
	void R() {//Read Shared memory
		if (!ERRCHK()) {

			pBuf = (LPTSTR)MapViewOfFile(hMapFile, // handle to map object
				FILE_MAP_ALL_ACCESS,  // read/write permission
				0,
				0,
				SM_SIZE);



			RXMSG = CT2A(pBuf);
		}

		//UnmapViewOfFile(pBuf);

		//CloseHandle(hMapFile);



	}//Read Shared memory

	void W() {//Write to shared memory


		static const int sSize = TXMSG.size() + 1;
		TCHAR szMsg[/*sSize*/1024];
		_tcscpy_s(szMsg, CA2T(TXMSG.c_str()));

		if (!ERRCHK()) {

			CopyMemory((PVOID)pBuf, szMsg, (_tcslen(szMsg) * sizeof(TCHAR)));
			//_getch();
			//UnmapViewOfFile(pBuf);
			//CloseHandle(hMapFile);
		}
	}//Write to shared memory

	void W(std::string mesg) {//Write to shared memory
		TXMSG = mesg;
		W();
	}//Write to shared memory
};

#endif // SML_H_INCLUDED
